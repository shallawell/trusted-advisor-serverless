#!/bin/bash
#update these
BUCKET=yourPublicBucket
EMAIL=user@example.com
REGION=us-east-1
FREQ="cron(15 10 ? * 6L *)"

# start
#Package you SAM application
echo "Starting Cloudformation packaging for "
aws cloudformation package --template-file template.yml --output-template-file packaged-ta-sam-template.yml --s3-bucket $BUCKET --region $REGION
#Deploy you SAM application
echo "Starting Cloudformation deploy for $STACKNAME"
STACKNAME=$USER-TrusterAdvisor-Checks
aws cloudformation deploy --template-file packaged-ta-sam-template.yml --stack-name $STACKNAME --parameter-overrides RunFrequency=$FREQ EmailAddress=$EMAIL --capabilities CAPABILITY_IAM --region $REGION
echo "done"
