# Trusted Advisor - regular scan and output

##Synopsis

This application will automatically query the Trusted Advisor API for results.

Subscription to the created SNS topic is created via a provide EmailAddress parameter.

## Deployment

*  edit cfnPackageDeploySAM.sh   
Update these variables;
```
BUCKET=yourPublicBucket
EMAIL=user@example.com
FREQ="cron(15 10 ? * 6L *)"
```
Make sure to use Lambda schedule formats. 

Run it
```
./cfnPackageDeploySAM.sh
```

### Alternative deployment
The easiest way to deploy it is by using the Serverless Application Repository. Search for it there and install. <br>
Alternatively you can run the provided `packaged-sam-template.yml` yourself. Either way, there are 2 parameters that can be provided:

*   RunFrequency: When should the applications run checks? 
*   EmailAddress: Used for notifications

##Lambda test
Use this as a simple test, to trigger the lambda function manually.
```
{
  "RunFrequency": "cron(* * * * * *)",
  "EmailAddress": "user@example.com"
}
```

## Trigger
This should be configured to run on a schedule. Cron format could be 10.15am on Fridays(15 10 * 6L *)

### Author
@shallawell
